FROM openjdk:8
ADD target/management-service.jar management-service.jar
EXPOSE 8082
RUN mkdir /images
ENTRYPOINT ["java", "-jar", "management-service.jar"]