package com.mor.managementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableDiscoveryClient
@RibbonClient(name = "management-service")
@EnableFeignClients
@EnableOAuth2Sso
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableCaching
public class ManagementServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ManagementServiceApplication.class, args);
  }

}
