package com.mor.managementservice.constant;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TimelineEnum {
  WEEK(0, "week"),
  MONTH(1, "month"),
  DAY(2, "day");

  private final int value;

  private final String timeline;

  private static final Map<String, Integer> BY_TIMELINE = new HashMap<>();
  private static final Map<Integer, String> BY_VALUE = new HashMap<>();

  static {
    for (TimelineEnum timelineEnum: values()) {
      BY_TIMELINE.put(timelineEnum.timeline, timelineEnum.value);
      BY_VALUE.put(timelineEnum.value, timelineEnum.timeline);
    }
  }
  public static int getValueByTimeline(String timeline) {
    return BY_TIMELINE.get(timeline);
  }
  public static String getTimelineByValue(int value) {
    return BY_VALUE.get(value);
  }
}
