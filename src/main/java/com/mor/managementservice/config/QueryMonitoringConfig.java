package com.mor.managementservice.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "hibernate")
@Getter
@Setter
@NoArgsConstructor
public class QueryMonitoringConfig {

  private Integer maxShared;

  private Integer maxReceivingShared;

  private Integer queryThreshold;

}
