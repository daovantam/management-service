package com.mor.managementservice.internal;

import com.mor.managementservice.factory.GeneralResponse;
import feign.hystrix.FallbackFactory;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ServiceClientFallBackFactory implements FallbackFactory<AuthServiceClient> {

  @Override
  public AuthServiceClient create(Throwable throwable) {
    return new AuthServiceClient() {
      @Override
      public ResponseEntity<GeneralResponse<List<String>>> getAllHostForSale(String name) {
        throw new RuntimeException(throwable);
      }
    };
  }
}
