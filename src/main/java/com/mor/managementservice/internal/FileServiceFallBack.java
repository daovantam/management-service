package com.mor.managementservice.internal;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class FileServiceFallBack implements FallbackFactory<FileServiceClient> {

  @Override
  public FileServiceClient create(Throwable throwable) {
    throw new RuntimeException(throwable);
  }
}
