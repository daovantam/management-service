package com.mor.managementservice.internal;

import com.mor.managementservice.factory.GeneralResponse;
import java.util.List;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "auth-service/uaa", fallbackFactory = ServiceClientFallBackFactory.class)
@RibbonClient(name = "auth-service")
public interface AuthServiceClient {

  @GetMapping(value = "api/v1/sale/{name}", consumes = "application/json")
  ResponseEntity<GeneralResponse<List<String>>> getAllHostForSale(
      @PathVariable("name") String name);
}
