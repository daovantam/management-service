package com.mor.managementservice.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Entity
@Table(name = "district")
@ToString
public class District implements Serializable {

  @Id
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "province_id", insertable = false, updatable = false)
  private String provinceId;

  @Column(name = "type")
  private String type;

  @ManyToOne
  @JoinColumn(name = "province_id")
  private Province province;
}
