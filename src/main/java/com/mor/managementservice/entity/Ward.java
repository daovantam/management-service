package com.mor.managementservice.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "ward")
@Setter
@Getter
@ToString
public class Ward implements Serializable {

  @Id
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "district_id", updatable = false, insertable = false)
  private String districtId;

  @Column(name = "type")
  private String type;

  @ManyToOne
  @JoinColumn(name = "district_id")
  private District district;
}
