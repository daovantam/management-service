package com.mor.managementservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "customer")
@Setter
@Getter
public class Customer extends BaseEntity {

  private String name;

  private String phone;

  @Column(name = "house_id", insertable = false, updatable = false)
  private String houseId;

  private int status;

  @Column(name = "host_name")
  private String hostName;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "house_id")
  private HouseForRent houseForRent;
}
