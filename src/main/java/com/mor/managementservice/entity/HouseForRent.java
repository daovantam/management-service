package com.mor.managementservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "house_for_rent")
@Setter
@Getter
@ToString
public class HouseForRent extends BaseEntity implements Serializable {

  @Column(name = "title", nullable = false)
  private String title;

  @Column(name = "price")
  private Double price;

  @Column(name = "acreage")
  private float acreage;

  @Column(name = "number_of_bedroom")
  private int numberOfBedroom;

  @Column(name = "timeline")
  private int timeline;

  @Column(name = "status")
  private boolean status;

  @Column(name = "province_id")
  private String provinceId;

  @Column(name = "district_id")
  private String districtId;

  @Column(name = "category_id", insertable = false, updatable = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID categoryId;

  @Column(name = "ward_id", insertable = false, updatable = false)
  private String wardId;

  @Column(name = "images_path")
  private String imagesPath;

  @Column(name = "description")
  private String description;

  @ManyToOne
  @JoinColumn(name = "category_id")
  @JsonIgnore
  private Category category;

  @ManyToOne
  @JoinColumn(name = "ward_id")
  private Ward ward;
}
