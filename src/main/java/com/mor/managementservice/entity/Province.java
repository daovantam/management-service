package com.mor.managementservice.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "province")
@Setter
@Getter
@ToString
public class Province implements Serializable {

  @Id
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "type")
  private String type;
}
