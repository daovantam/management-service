package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.HouseCreateDto;
import com.mor.managementservice.dto.response.HouseResponseDto;
import com.mor.managementservice.dto.response.ProvinceResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteria;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.HouseUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.service.HouseService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/house")
@Slf4j
public class HouseController {

  public final ResponseFactory responseFactory;
  public final HouseService houseService;

  public HouseController(ResponseFactory responseFactory, HouseService houseService) {
    this.responseFactory = responseFactory;
    this.houseService = houseService;
  }

  @ApiOperation(value = "create House", response = HouseResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createHouse(Principal principal,
      @RequestParam("title") String title,
      @RequestParam("price") Double price,
      @RequestParam("acreage") float acreage,
      @RequestParam("numberOfBedroom") int numberOfBedroom,
      @RequestParam("wardId") String wardId,
      @RequestParam("categoryId") UUID categoryId,
      @RequestParam("images") List<MultipartFile> files,
      @RequestParam("description") String description,
      @RequestParam("timeline") String timeline
  ) throws IOException {
    HouseCreateDto houseCreateDto = new HouseCreateDto(title, price, acreage, numberOfBedroom,
        wardId, timeline, categoryId, files, "", description);
    log.info("create House with data: {} by {}", houseCreateDto, principal.getName());
    HouseResponseDto response = houseService.create(houseCreateDto, principal);
    log.info("create House successfully with id: {}", response.getId());
    return responseFactory.created(response, HouseResponseDto.class);
  }

  @ApiOperation(value = "update House", response = HouseResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully update House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updateHouse(@RequestBody HouseUpdateDto houseUpdateDto,
      Principal principal) {
    log.info("start update House with data: {} by {}", houseUpdateDto, principal.getName());
    HouseResponseDto response = houseService.update(houseUpdateDto, principal);
    log.info("update successfully - id updated: {}", response.getId());
    return responseFactory.success(response, HouseResponseDto.class);
  }

  @ApiOperation(value = "delete House")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully delete House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteHouse(@PathVariable("id") UUID id, Principal principal) {
    log.info("start delete House with id: {} by {}", id, principal.getName());
    houseService.deleteById(id);
    log.info("delete House successfully with id: {}", id);
    return responseFactory.success();
  }

  @ApiOperation(value = "get details House", response = ProvinceResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully get details House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping("{id}")
  public ResponseEntity<Object> getDetailsHouse(@PathVariable("id") UUID id,
      Principal principal) {
    log.info("start get details House with id: {} by {}", id, principal.getName());
    HouseResponseDto response = houseService.getDetailsById(id);
    log.info("get details successfully - data: {}", response);

    return responseFactory.success(response, HouseResponseDto.class);
  }


  @ApiOperation(value = "search Criteria House", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  public ResponseEntity<Object> search(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<HouseResponseDto> dtoResponse = houseService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }

  @ApiOperation(value = "search Criteria House by Host", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria House"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/host/search")
  public ResponseEntity<Object> searchByHost(
      @RequestBody @Valid SearchCriteriaDto searchCriteriaDto, Principal principal) {
    SearchCriteria searchCriteria = new SearchCriteria();
    searchCriteria.setKey("createdBy");
    searchCriteria.setOperation(":");
    searchCriteria.setValue(principal.getName());
    searchCriteria.setOrPredicate(false);
    searchCriteriaDto.getMatching().add(searchCriteria);
    SearchDtoResponse<HouseResponseDto> dtoResponse = houseService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }
}
