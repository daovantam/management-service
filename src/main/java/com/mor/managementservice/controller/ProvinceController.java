package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.ProvinceCreateDto;
import com.mor.managementservice.dto.response.ProvinceResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.ProvinceUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.service.ProvinceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/province")
@Slf4j
public class ProvinceController {

  public final ResponseFactory responseFactory;

  public final ProvinceService provinceService;

  public ProvinceController(ResponseFactory responseFactory,
      ProvinceService provinceService) {
    this.responseFactory = responseFactory;
    this.provinceService = provinceService;
  }

  @ApiOperation(value = "create Province", response = ProvinceResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create Province"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createProvince(
      @RequestBody @Valid ProvinceCreateDto provinceCreateDto,
      Principal principal) throws IOException {
    log.info("create Province with data: {} by {}", provinceCreateDto, principal.getName());
    ProvinceResponseDto response = provinceService.create(provinceCreateDto, principal);
    log.info("create Province successfully with id: {}", response.getId());
    return responseFactory.created(response, ProvinceResponseDto.class);
  }

  @ApiOperation(value = "update Province", response = ProvinceResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully update Province"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updateProvince(
      @RequestBody @Valid ProvinceUpdateDto provinceUpdateDto,
      Principal principal) {
    log.info("start update Province with data: {} by {}", provinceUpdateDto, principal.getName());
    ProvinceResponseDto response = provinceService.update(provinceUpdateDto, principal);
    log.info("update successfully - id updated: {}", response.getId());
    return responseFactory.success(response, ProvinceResponseDto.class);
  }

//  @ApiOperation(value = "delete Province")
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully delete Province"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @DeleteMapping("/{id}")
//  public ResponseEntity<Object> deleteProvince(@PathVariable("id") UUID id, Principal principal) {
//    log.info("start delete Province with id: {} by {}", id, principal.getName());
//    provinceService.deleteById(id);
//    log.info("delete Province successfully with id: {}", id);
//    return responseFactory.success();
//  }
//
//  @ApiOperation(value = "get details Category", response = ProvinceResponseDto.class)
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully get details Category"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @GetMapping("{id}")
//  public ResponseEntity<Object> getDetailsProvince(@PathVariable("id")UUID id, Principal principal) {
//    log.info("start get details Province with id: {} by {}", id, principal.getName());
//    ProvinceResponseDto response = provinceService.getDetailsById(id);
//    log.info("get details successfully - data: {}", response);
//
//    return responseFactory.success(response, ProvinceResponseDto.class);
//  }

  @ApiOperation(value = "search Criteria Province", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria Province"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  public ResponseEntity<Object> search(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<ProvinceResponseDto> dtoResponse = provinceService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }
}
