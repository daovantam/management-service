package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.CustomerCreateDto;
import com.mor.managementservice.dto.response.CustomerResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.CustomerUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.internal.AuthServiceClient;
import com.mor.managementservice.service.CustomerService;
import java.io.IOException;
import java.security.Principal;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {

  private final ResponseFactory responseFactory;

  private final CustomerService customerService;

  private final AuthServiceClient authServiceClient;

  @Autowired
  public CustomerController(ResponseFactory responseFactory,
      CustomerService customerService,
      AuthServiceClient authServiceClient) {
    this.responseFactory = responseFactory;
    this.customerService = customerService;
    this.authServiceClient = authServiceClient;
  }

  @PostMapping
  public ResponseEntity<Object> createCustomer(@RequestBody CustomerCreateDto customerCreateDto,
      Principal principal)
      throws IOException {
    CustomerResponseDto customer = customerService.create(customerCreateDto, principal);
    return responseFactory.created(customer, CustomerResponseDto.class);
  }

  @PreAuthorize("hasRole('ROLE_RM')")
  @PutMapping
  public ResponseEntity<Object> update(@RequestBody CustomerUpdateDto customerUpdateDto,
      Principal principal) {
    CustomerResponseDto responseDto = customerService.update(customerUpdateDto, principal);
    return responseFactory.success(responseDto, CustomerResponseDto.class);
  }

  @PreAuthorize("hasRole('ROLE_RM')")
  @PostMapping("/search")
  public ResponseEntity<Object> search(@RequestBody SearchCriteriaDto searchCriteriaDto,
      Principal principal) {
    SearchDtoResponse<CustomerResponseDto> response = customerService
        .searchCustomer(searchCriteriaDto, principal);

    return responseFactory.success(response, SearchDtoResponse.class);
  }

  @PreAuthorize("hasRole('ROLE_RM')")
  @GetMapping("/{id}")
  public ResponseEntity<Object> get(@PathVariable("id") UUID id) {
    CustomerResponseDto responseDto = customerService.getDetailsById(id);

    return responseFactory.success(responseDto, CustomerResponseDto.class);
  }

  @PreAuthorize("hasRole('ROLE_RM')")
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable("id") UUID id) {
    customerService.deleteById(id);
    return responseFactory.success();
  }
}
