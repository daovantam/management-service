package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.CategoryCreateDto;
import com.mor.managementservice.dto.response.CategoryResponseDto;
import com.mor.managementservice.dto.update.CategoryUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/category")
@Slf4j
public class CategoryController {

  public final ResponseFactory responseFactory;
  public final CategoryService categoryService;

  @Autowired
  public CategoryController(ResponseFactory responseFactory,
      CategoryService categoryService) {
    this.responseFactory = responseFactory;
    this.categoryService = categoryService;
  }

  @ApiOperation(value = "create Category", response = CategoryResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create Category"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createCategory(@RequestParam("name") String name,
      @RequestParam("image") MultipartFile file,
      @RequestParam("description") String description,
      Principal principal) throws IOException {
    CategoryCreateDto categoryCreateDto = new CategoryCreateDto(name, file, description);
    log.info("create category with data: {} by {}", categoryCreateDto, principal.getName());
    CategoryResponseDto response = categoryService.create(categoryCreateDto, principal);
    log.info("create category successfully with id: {}", response.getId());
    return responseFactory.created(response, CategoryResponseDto.class);
  }

  @ApiOperation(value = "update Category", response = CategoryResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully update Category"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updateCategory(
      @RequestBody @Valid CategoryUpdateDto categoryUpdateDto,
      Principal principal) {
    log.info("start update category with data: {} by {}", categoryUpdateDto, principal.getName());
    CategoryResponseDto response = categoryService.update(categoryUpdateDto, principal);
    log.info("update successfully - id updated: {}", response.getId());
    return responseFactory.success(response, CategoryResponseDto.class);
  }

  @ApiOperation(value = "delete Category")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully delete Category"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteCategory(@PathVariable("id") UUID id, Principal principal) {
    log.info("start delete category with id: {} by {}", id, principal.getName());
    categoryService.deleteById(id);
    log.info("delete category successfully with id: {}", id);
    return responseFactory.success();
  }

  @ApiOperation(value = "get details Category", response = CategoryResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully get details Category"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping("{id}")
  public ResponseEntity<Object> getDetailsCategory(@PathVariable("id") UUID id,
      Principal principal) {
    log.info("start get details category with id: {} by {}", id, principal.getName());
    CategoryResponseDto response = categoryService.getDetailsById(id);
    log.info("get details successfully - data: {}", response);

    return responseFactory.success(response, CategoryResponseDto.class);
  }

  @ApiOperation(value = "get List Category", response = CategoryResponseDto.class, responseContainer = "List")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully get list Category"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping
  public ResponseEntity<Object> getListCategory() {
    List<CategoryResponseDto> response = categoryService.getCategoryList();
    return responseFactory.success(response, List.class);
  }
}
