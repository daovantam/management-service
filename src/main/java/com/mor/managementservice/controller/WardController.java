package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.WardCreateDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.WardResponseDto;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.WardUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.service.WardService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/ward")
@Slf4j
public class WardController {

  public final ResponseFactory responseFactory;
  public final WardService wardService;

  public WardController(ResponseFactory responseFactory,
      WardService wardService) {
    this.responseFactory = responseFactory;
    this.wardService = wardService;
  }

  @ApiOperation(value = "create Ward", response = WardResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create Ward"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createWard(
      @RequestBody @Valid WardCreateDto wardCreateDto,
      Principal principal) throws IOException {
    log.info("create Ward with data: {} by {}", wardCreateDto, principal.getName());
    WardResponseDto response = wardService.create(wardCreateDto, principal);
    log.info("create Ward successfully with id: {}", response.getId());
    return responseFactory.created(response, WardResponseDto.class);
  }

  @ApiOperation(value = "update Ward", response = WardResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully update Ward"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updateWard(
      @RequestBody @Valid WardUpdateDto wardUpdateDto,
      Principal principal) {
    log.info("start update Ward with data: {} by {}", wardUpdateDto, principal.getName());
    WardResponseDto response = wardService.update(wardUpdateDto, principal);
    log.info("update successfully - id updated: {}", response.getId());
    return responseFactory.success(response, WardResponseDto.class);
  }

//  @ApiOperation(value = "delete Ward")
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully delete Ward"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @DeleteMapping("/{id}")
//  public ResponseEntity<Object> deleteWard(@PathVariable("id") UUID id, Principal principal) {
//    log.info("start delete Ward with id: {} by {}", id, principal.getName());
//    wardService.deleteById(id);
//    log.info("delete Ward successfully with id: {}", id);
//    return responseFactory.success();
//  }

//  @ApiOperation(value = "get details Ward", response = ProvinceResponseDto.class)
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully get details Ward"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @GetMapping("{id}")
//  public ResponseEntity<Object> getDetailsWard(@PathVariable("id") UUID id,
//      Principal principal) {
//    log.info("start get details Ward with id: {} by {}", id, principal.getName());
//    WardResponseDto response = wardService.getDetailsById(id);
//    log.info("get details successfully - data: {}", response);
//
//    return responseFactory.success(response, WardResponseDto.class);
//  }


  @ApiOperation(value = "search Criteria Ward", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria Ward"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  public ResponseEntity<Object> search(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<WardResponseDto> dtoResponse = wardService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }
}
