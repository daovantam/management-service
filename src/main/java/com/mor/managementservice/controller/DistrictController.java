package com.mor.managementservice.controller;

import com.mor.managementservice.dto.create.DistrictCreateDto;
import com.mor.managementservice.dto.response.DistrictResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.DistrictUpdateDto;
import com.mor.managementservice.factory.ResponseFactory;
import com.mor.managementservice.service.DistrictService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/district")
@Slf4j
public class DistrictController {

  public final ResponseFactory responseFactory;
  public final DistrictService districtService;

  public DistrictController(ResponseFactory responseFactory,
      DistrictService districtService) {
    this.responseFactory = responseFactory;
    this.districtService = districtService;
  }

  @ApiOperation(value = "create District", response = DistrictResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create District"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createDistrict(
      @RequestBody @Valid DistrictCreateDto districtCreateDto,
      Principal principal) throws IOException {
    log.info("create district with data: {} by {}", districtCreateDto, principal.getName());
    DistrictResponseDto response = districtService.create(districtCreateDto, principal);
    log.info("create district successfully with id: {}", response.getId());
    return responseFactory.created(response, DistrictResponseDto.class);
  }

  @ApiOperation(value = "update District", response = DistrictResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully update District"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updateDistrict(
      @RequestBody @Valid DistrictUpdateDto districtUpdateDto,
      Principal principal) {
    log.info("start update District with data: {} by {}", districtUpdateDto, principal.getName());
    DistrictResponseDto response = districtService.update(districtUpdateDto, principal);
    log.info("update successfully - id updated: {}", response.getId());
    return responseFactory.success(response, DistrictResponseDto.class);
  }

//  @ApiOperation(value = "delete District")
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully delete District"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @DeleteMapping("/{id}")
//  public ResponseEntity<Object> deleteDistrict(@PathVariable("id") UUID id, Principal principal) {
//    log.info("start delete District with id: {} by {}", id, principal.getName());
//    districtService.deleteById(id);
//    log.info("delete District successfully with id: {}", id);
//    return responseFactory.success();
//  }
//
//  @ApiOperation(value = "get details District", response = ProvinceResponseDto.class)
//  @ApiResponses(value = {
//      @ApiResponse(code = 200, message = "Successfully get details District"),
//      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//      @ApiResponse(code = 400, message = "Invalid params, please try again")})
//  @GetMapping("{id}")
//  public ResponseEntity<Object> getDetailsDistrict(@PathVariable("id") UUID id,
//      Principal principal) {
//    log.info("start get details District with id: {} by {}", id, principal.getName());
//    DistrictResponseDto response = districtService.getDetailsById(id);
//    log.info("get details successfully - data: {}", response);
//
//    return responseFactory.success(response, DistrictResponseDto.class);
//  }


  @ApiOperation(value = "search Criteria District", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria District"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  public ResponseEntity<Object> search(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<DistrictResponseDto> dtoResponse = districtService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }
}
