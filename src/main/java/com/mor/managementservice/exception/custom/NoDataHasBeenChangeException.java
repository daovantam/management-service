package com.mor.managementservice.exception.custom;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import lombok.Getter;

@Getter
public class NoDataHasBeenChangeException extends RuntimeException {

  private final String code;

  public NoDataHasBeenChangeException(ResponseStatusCodeEnum massage) {
    super(massage.getMessage());
    this.code = massage.getCode();
  }
}
