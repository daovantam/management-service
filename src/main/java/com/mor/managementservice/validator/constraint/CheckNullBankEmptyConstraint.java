package com.mor.managementservice.validator.constraint;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckNullBankEmptyConstraint implements
    ConstraintValidator<NotNullBlankEmpty, String> {

  @Override
  public void initialize(NotNullBlankEmpty constraintAnnotation) {

  }

  @Override
  public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
    if (field != null) {
      if (field.isEmpty()) {
        return false;
      } else if (field.matches("(\\s)+")) {
        return false;
      }
      return true;
    }
    return false;
  }
}
