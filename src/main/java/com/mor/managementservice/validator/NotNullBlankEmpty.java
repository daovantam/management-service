package com.mor.managementservice.validator;

import com.mor.managementservice.validator.constraint.CheckNullBankEmptyConstraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = CheckNullBankEmptyConstraint.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotNullBlankEmpty {

  String message() default "field input incorrect";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
