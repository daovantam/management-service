package com.mor.managementservice.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponseDto {

  private String name;
  private String phone;
  private String titleHouse;
  private String hostName;
}
