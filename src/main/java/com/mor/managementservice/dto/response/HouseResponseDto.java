package com.mor.managementservice.dto.response;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@AllArgsConstructor
@Builder
public class HouseResponseDto {

  private UUID id;

  private String title;

  private Double price;

  private float acreage;

  private int numberOfBedroom;

  private String timeline;

  private String wardName;

  private String wardId;

  private boolean status;

  private String districtName;

  private String districtId;

  private String provinceName;

  private String provinceId;

  private String categoryName;

  private UUID categoryId;

  private List<String> imagesPath;

  private String description;
}
