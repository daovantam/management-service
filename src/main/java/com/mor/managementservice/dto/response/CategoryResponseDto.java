package com.mor.managementservice.dto.response;

import com.mor.managementservice.dto.update.CategoryUpdateDto;
import java.io.Serializable;
import java.sql.Timestamp;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class CategoryResponseDto extends CategoryUpdateDto implements Serializable {

  public CategoryResponseDto() {
    super();
  }

  private Timestamp updatedTimestamp;
  private String imagePath;
  private String updateBy;
}
