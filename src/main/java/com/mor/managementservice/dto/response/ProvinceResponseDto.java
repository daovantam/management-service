package com.mor.managementservice.dto.response;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProvinceResponseDto implements Serializable {

  private String id;
  private String name;
  private String type;
}
