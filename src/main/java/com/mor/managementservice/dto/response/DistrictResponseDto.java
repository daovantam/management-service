package com.mor.managementservice.dto.response;

import java.io.Serializable;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class DistrictResponseDto implements Serializable {

  private String id;

  private String name;

  private String provinceName;

  private Timestamp updatedTimestamp;

  private String updatedBy;
}
