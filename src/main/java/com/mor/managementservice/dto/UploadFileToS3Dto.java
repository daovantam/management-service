package com.mor.managementservice.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class UploadFileToS3Dto implements Serializable {

  private String folderName;

  private String fileName;

  private byte[] file;
}
