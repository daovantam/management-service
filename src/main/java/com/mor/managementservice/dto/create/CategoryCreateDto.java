package com.mor.managementservice.dto.create;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCreateDto implements Serializable {

  @NotNullBlankEmpty(message = "category name incorrect")
  private String name;

  private MultipartFile image;
  private String description;
}
