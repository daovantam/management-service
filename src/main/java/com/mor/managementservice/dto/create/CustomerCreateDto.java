package com.mor.managementservice.dto.create;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerCreateDto {

  private String name;

  private String phone;

  private UUID houseId;
}
