package com.mor.managementservice.dto.create;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DistrictCreateDto implements Serializable {

  @NotNullBlankEmpty(message = "district name incorrect")
  private String name;

  @NotNull(message = "provinceId must not be null")
  private String provinceId;
}
