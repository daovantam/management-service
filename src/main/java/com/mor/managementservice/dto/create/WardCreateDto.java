package com.mor.managementservice.dto.create;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class WardCreateDto {

  @NotNullBlankEmpty(message = "Ward name incorrect")
  private String name;

  @NotNull
  private String districtId;
}
