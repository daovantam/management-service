package com.mor.managementservice.dto.create;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class HouseCreateDto {

  private String title;

  private Double price;

  private float acreage;

  private int numberOfBedroom;

  private String wardId;

  private String timeline;

  private UUID categoryId;

  private List<MultipartFile> files;
  private String filesPath;
  private String description;
}
