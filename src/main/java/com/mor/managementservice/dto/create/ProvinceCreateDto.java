package com.mor.managementservice.dto.create;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProvinceCreateDto implements Serializable {

  @NotNullBlankEmpty(message = "Province name incorrect")
  private String name;
}
