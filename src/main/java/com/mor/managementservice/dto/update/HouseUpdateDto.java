package com.mor.managementservice.dto.update;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HouseUpdateDto {

  private UUID id;

  private String title;

  private Double price;

  private float acreage;

  private int numberOfBedroom;

  private String timeline;

  private String wardId;

  private UUID categoryId;

  private String description;
}
