package com.mor.managementservice.dto.update;

import com.mor.managementservice.dto.create.CategoryCreateDto;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CategoryUpdateDto extends CategoryCreateDto implements Serializable {

  @NotNull
  private UUID id;
}
