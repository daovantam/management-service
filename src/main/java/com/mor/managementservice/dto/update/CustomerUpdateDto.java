package com.mor.managementservice.dto.update;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerUpdateDto {

  private UUID id;
  private int status;
}
