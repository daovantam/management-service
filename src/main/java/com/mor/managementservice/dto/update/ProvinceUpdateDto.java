package com.mor.managementservice.dto.update;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProvinceUpdateDto implements Serializable {

  @NotNull
  private String id;

  @NotNullBlankEmpty(message = "Province name incorrect")
  private String name;
}
