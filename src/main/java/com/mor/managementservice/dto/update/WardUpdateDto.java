package com.mor.managementservice.dto.update;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class WardUpdateDto {

  private String id;

  private String name;

  private String districtId;
}
