package com.mor.managementservice.dto.update;

import com.mor.managementservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DistrictUpdateDto implements Serializable {

  @NotNull
  private String id;

  @NotNullBlankEmpty(message = "District name incorrect")
  private String name;

  @NotNull
  private String provinceId;
}
