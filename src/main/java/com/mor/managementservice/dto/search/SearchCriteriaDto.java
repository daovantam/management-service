package com.mor.managementservice.dto.search;

import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SearchCriteriaDto implements Serializable {

  @NotNull(message = "page must not null")
  @Min(value = 0, message = "Min value of field page is 0 - ReInput please")
  private Integer page;
  @NotNull(message = "page_size must not null")
  @Min(value = 1, message = "Min value of field page_size is 1 - ReInput please")
  private Integer pageSize;

  private String orderBy;

  private boolean direction;

  private List<SearchCriteria> matching;
}
