package com.mor.managementservice.service;

import java.util.UUID;

public interface UUIDService<R> {

  void deleteById(UUID id);

  R getDetailsById(UUID id);
}
