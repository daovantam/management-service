package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.CategoryCreateDto;
import com.mor.managementservice.dto.response.CategoryResponseDto;
import com.mor.managementservice.dto.update.CategoryUpdateDto;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface CategoryService extends
    BaseService<CategoryCreateDto, CategoryUpdateDto, CategoryResponseDto>,
    UUIDService<CategoryResponseDto> {

  List<CategoryResponseDto> getCategoryList();
}
