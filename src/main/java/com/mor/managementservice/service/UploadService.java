package com.mor.managementservice.service;

import java.io.IOException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface UploadService {
  String sendDataToFileService(MultipartFile file) throws IOException;
}
