package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.HouseCreateDto;
import com.mor.managementservice.dto.response.HouseResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.HouseUpdateDto;
import com.mor.managementservice.entity.HouseForRent;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.repository.HouseRepository;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.HouseService;
import com.mor.managementservice.service.SearchService;
import com.mor.managementservice.service.UploadService;
import com.mor.managementservice.utils.converter.HouseConverter;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class HouseServiceImpl implements HouseService {

  private final HouseConverter houseConverter;
  private final HouseRepository houseRepository;
  private final SearchService<HouseForRent> searchService;
  private final UploadService uploadService;
  private final SpecificationBuilder builder = new SpecificationBuilder(HouseForRent.class);

  @Autowired
  public HouseServiceImpl(HouseConverter houseConverter,
      HouseRepository houseRepository,
      SearchService<HouseForRent> searchService,
      UploadService uploadService) {
    this.houseConverter = houseConverter;
    this.houseRepository = houseRepository;
    this.searchService = searchService;
    this.uploadService = uploadService;
  }

  @Override
  public HouseResponseDto create(HouseCreateDto houseCreateDto, Principal principal)
      throws IOException {
    StringBuilder imagePath = new StringBuilder();
    for (MultipartFile multipartFile : houseCreateDto.getFiles()) {
      if (imagePath.length() == 0) {
        imagePath.append(uploadService.sendDataToFileService(multipartFile));
      } else {
        imagePath.append(";").append(uploadService.sendDataToFileService(multipartFile));
      }
    }
    houseCreateDto.setFilesPath(imagePath.toString());
    HouseForRent houseForRent = houseConverter.createDtoToEntity(houseCreateDto, principal);
    HouseForRent created = houseRepository.save(houseForRent);
    return houseConverter.entityToDtoResponse(created);
  }

  @Override
  public HouseResponseDto update(HouseUpdateDto houseUpdateDto, Principal principal) {
    HouseForRent houseForRent = houseRepository.findById(houseUpdateDto.getId()).orElseThrow(
        EntityNotFoundException::new);
    boolean checkUpdate = houseConverter.updateDtoToEntity(houseForRent, houseUpdateDto, principal);
    if (checkUpdate) {
      HouseForRent updated = houseRepository.save(houseForRent);
      return houseConverter.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

  @Override
  public void deleteById(UUID id) {
    houseRepository.deleteById(id);
  }

  @Override
  public HouseResponseDto getDetailsById(UUID id) {
    HouseForRent houseForRent = houseRepository.findById(id).orElseThrow(
        EntityNotFoundException::new);
    return houseConverter.entityToDtoResponse(houseForRent);
  }

  @Override
  public SearchDtoResponse<HouseResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<HouseResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<HouseForRent> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<HouseForRent> wards = houseRepository.findAll(provinceSearchPreProcess.getSpecification(),
        provinceSearchPreProcess.getPageable());
    List<HouseResponseDto> provinceResponseDtoList = wards.getContent().stream().parallel()
        .map(houseConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(wards.getTotalPages());
    searchDtoResponse.setTotalElement(wards.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }
}
