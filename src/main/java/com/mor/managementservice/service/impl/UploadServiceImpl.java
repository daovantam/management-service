package com.mor.managementservice.service.impl;

import com.mor.managementservice.dto.UploadFileToS3Dto;
import com.mor.managementservice.internal.FileServiceClient;
import com.mor.managementservice.service.UploadService;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class UploadServiceImpl implements UploadService {


  private final FileServiceClient fileServiceClient;
  private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
  private final String folderName = simpleDateFormat.format(new Date());

  @Autowired
  public UploadServiceImpl(FileServiceClient fileServiceClient) {
    this.fileServiceClient = fileServiceClient;
  }

  @Override
  public String sendDataToFileService(MultipartFile multipartFile) throws IOException {
    File file = new File("/images/" + multipartFile.getOriginalFilename());
    multipartFile.transferTo(file);
    UploadFileToS3Dto uploadFileToS3Dto = new UploadFileToS3Dto(folderName,
        multipartFile.getOriginalFilename(), FileUtils.readFileToByteArray(file));
    String result = Objects
        .requireNonNull(fileServiceClient.uploadFileToS3(uploadFileToS3Dto).getBody()).getData();
    if (file.delete()) {
      log.info("deleted {} in local", file.getName());
    }
    return result;
  }
}
