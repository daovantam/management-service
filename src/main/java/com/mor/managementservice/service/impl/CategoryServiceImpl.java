package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.CategoryCreateDto;
import com.mor.managementservice.dto.response.CategoryResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.CategoryUpdateDto;
import com.mor.managementservice.entity.Category;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.repository.CategoryRepository;
import com.mor.managementservice.service.CategoryService;
import com.mor.managementservice.service.UploadService;
import com.mor.managementservice.utils.converter.ConverterCategory;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

  public final CategoryRepository categoryRepository;

  public final ConverterCategory converterCategory;
  public final UploadService uploadService;

  @Autowired
  public CategoryServiceImpl(CategoryRepository categoryRepository,
      ConverterCategory converterCategory,
      UploadService uploadService) {
    this.categoryRepository = categoryRepository;
    this.converterCategory = converterCategory;
    this.uploadService = uploadService;
  }

  @Override
  public CategoryResponseDto create(CategoryCreateDto categoryCreateDto, Principal principal)
      throws IOException {
    String result = uploadService.sendDataToFileService(categoryCreateDto.getImage());
    Category category = converterCategory.createDtoToEntity(categoryCreateDto, principal);
    category.setImage(result);
    Category response = categoryRepository.save(category);
    return converterCategory.entityToDtoResponse(response);
  }

  @Override
  @CacheEvict(value = "category", key = "#categoryUpdateDto.id")
  public CategoryResponseDto update(CategoryUpdateDto categoryUpdateDto, Principal principal) {
    Category categoryExist = categoryRepository.findById(categoryUpdateDto.getId()).orElseThrow(
        EntityNotFoundException::new);
    boolean checkUpdate = converterCategory
        .updateDtoToEntity(categoryExist, categoryUpdateDto, principal);
    if (checkUpdate) {
      Category updated = categoryRepository.save(categoryExist);
      return converterCategory.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

  @Override
  public void deleteById(UUID id) {
    categoryRepository.deleteById(id);
  }

  @Override
  @Cacheable(value = "category", key = "#id")
  public CategoryResponseDto getDetailsById(UUID id) {
    Category category = categoryRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    return converterCategory.entityToDtoResponse(category);
  }

  @Override
  public SearchDtoResponse<CategoryResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    return null;
  }

  @Override
  public List<CategoryResponseDto> getCategoryList() {
    List<Category> categories = categoryRepository.findAll();
    return categories.stream().map(converterCategory::entityToDtoResponse)
        .collect(Collectors.toList());
  }
}
