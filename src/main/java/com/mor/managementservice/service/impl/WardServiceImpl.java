package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.WardCreateDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.response.WardResponseDto;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.WardUpdateDto;
import com.mor.managementservice.entity.Ward;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.repository.WardRepository;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.SearchService;
import com.mor.managementservice.service.WardService;
import com.mor.managementservice.utils.converter.WardConverter;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class WardServiceImpl implements WardService {

  private final WardConverter wardConverter;
  private final WardRepository wardRepository;
  private final SearchService<Ward> searchService;
  private final SpecificationBuilder builder = new SpecificationBuilder(Ward.class);

  @Autowired
  public WardServiceImpl(WardConverter wardConverter,
      WardRepository wardRepository,
      SearchService<Ward> searchService) {
    this.wardConverter = wardConverter;
    this.wardRepository = wardRepository;
    this.searchService = searchService;
  }

  @Override
  public WardResponseDto create(WardCreateDto wardCreateDto, Principal principal) {
    Ward ward = wardConverter.createDtoToEntity(wardCreateDto, principal);
    Ward created = wardRepository.save(ward);
    return wardConverter.entityToDtoResponse(created);
  }

  @Override
  public WardResponseDto update(WardUpdateDto wardUpdateDto, Principal principal) {
    Ward ward = wardRepository.findById(wardUpdateDto.getId())
        .orElseThrow(EntityNotFoundException::new);
    boolean checkUpdate = wardConverter.updateDtoToEntity(ward, wardUpdateDto, principal);
    if (checkUpdate) {
      Ward updated = wardRepository.save(ward);
      return wardConverter.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

//  @Override
//  public void deleteById(String id) {
//    wardRepository.deleteById(id);
//  }
//
//  @Override
//  public WardResponseDto getDetailsById(String id) {
//    Ward ward = wardRepository.findById(id).orElseThrow(EntityNotFoundException::new);
//    return wardConverter.entityToDtoResponse(ward);
//  }

  @Override
  public SearchDtoResponse<WardResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<WardResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<Ward> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<Ward> wards = wardRepository.findAll(provinceSearchPreProcess.getSpecification(),
        provinceSearchPreProcess.getPageable());
    List<WardResponseDto> provinceResponseDtoList = wards.getContent().stream().parallel()
        .map(wardConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(wards.getTotalPages());
    searchDtoResponse.setTotalElement(wards.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }
}
