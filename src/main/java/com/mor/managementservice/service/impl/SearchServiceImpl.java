package com.mor.managementservice.service.impl;

import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.SearchService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl<T> implements SearchService<T> {

  @Override
  public SearchPreProcess<T> search(SearchCriteriaDto searchCriteriaDto,
      SpecificationBuilder builder) {
    Sort orders;
    if (searchCriteriaDto.isDirection()) {
      orders = new Sort(Sort.Direction.DESC, searchCriteriaDto.getOrderBy());
    } else {
      orders = new Sort(Sort.Direction.ASC, searchCriteriaDto.getOrderBy());
    }

    Pageable pageable = PageRequest
        .of(searchCriteriaDto.getPage(), searchCriteriaDto.getPageSize(), orders);
    Specification<T> specification = builder.buildCriteria(searchCriteriaDto.getMatching());
    return new SearchPreProcess<>(pageable, specification);
  }
}
