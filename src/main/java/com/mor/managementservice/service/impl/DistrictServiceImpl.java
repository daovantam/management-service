package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.DistrictCreateDto;
import com.mor.managementservice.dto.response.DistrictResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.DistrictUpdateDto;
import com.mor.managementservice.entity.District;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.repository.DistrictRepository;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.DistrictService;
import com.mor.managementservice.service.SearchService;
import com.mor.managementservice.utils.converter.DistrictConverter;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class DistrictServiceImpl implements DistrictService {

  private final DistrictConverter districtConverter;
  private final DistrictRepository districtRepository;
  private final SearchService<District> searchService;
  private final SpecificationBuilder builder = new SpecificationBuilder(District.class);

  @Autowired
  public DistrictServiceImpl(
      DistrictConverter districtConverter,
      DistrictRepository districtRepository,
      SearchService<District> searchService) {
    this.districtConverter = districtConverter;
    this.districtRepository = districtRepository;
    this.searchService = searchService;
  }

  @Override
  public DistrictResponseDto create(DistrictCreateDto districtCreateDto, Principal principal) {
    District district = districtConverter.createDtoToEntity(districtCreateDto, principal);
    District created = districtRepository.save(district);
    return districtConverter.entityToDtoResponse(created);
  }

  @Override
  public DistrictResponseDto update(DistrictUpdateDto districtUpdateDto, Principal principal) {

    District districtExist = districtRepository.findById(districtUpdateDto.getId()).orElseThrow(
        EntityNotFoundException::new);
    boolean checkUpdate = districtConverter
        .updateDtoToEntity(districtExist, districtUpdateDto, principal);
    if (checkUpdate) {
      District updated = districtRepository.save(districtExist);
      return districtConverter.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

//  @Override
//  public void deleteById(UUID id) {
//    districtRepository.deleteById(id);
//  }
//
//  @Override
//  public DistrictResponseDto getDetailsById(UUID id) {
//    District districtExist = districtRepository.findById(id).orElseThrow(
//        EntityNotFoundException::new);
//    return districtConverter.entityToDtoResponse(districtExist);
//  }

  @Override
  public SearchDtoResponse<DistrictResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<DistrictResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<District> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<District> districts = districtRepository
        .findAll(provinceSearchPreProcess.getSpecification(),
            provinceSearchPreProcess.getPageable());
    List<DistrictResponseDto> provinceResponseDtoList = districts.getContent().stream().parallel()
        .map(districtConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(districts.getTotalPages());
    searchDtoResponse.setTotalElement(districts.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }
}
