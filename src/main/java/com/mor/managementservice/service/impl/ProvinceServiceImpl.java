package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.ProvinceCreateDto;
import com.mor.managementservice.dto.response.ProvinceResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.ProvinceUpdateDto;
import com.mor.managementservice.entity.Province;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.repository.ProvinceRepository;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.ProvinceService;
import com.mor.managementservice.service.SearchService;
import com.mor.managementservice.utils.converter.ProvinceConverter;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class ProvinceServiceImpl implements ProvinceService {

  private final ProvinceRepository provinceRepository;

  private final SearchService<Province> searchService;

  private final SpecificationBuilder builder = new SpecificationBuilder(Province.class);

  private final ProvinceConverter provinceConverter;

  @Autowired
  public ProvinceServiceImpl(ProvinceRepository provinceRepository,
      SearchService<Province> searchService,
      ProvinceConverter provinceConverter) {
    this.provinceRepository = provinceRepository;
    this.searchService = searchService;
    this.provinceConverter = provinceConverter;
  }

  @Override
  public ProvinceResponseDto create(ProvinceCreateDto provinceCreateDto, Principal principal) {
    Province province = provinceConverter.createDtoToEntity(provinceCreateDto, principal);
    Province created = provinceRepository.save(province);
    return provinceConverter.entityToDtoResponse(created);
  }

  @Override
  public ProvinceResponseDto update(ProvinceUpdateDto provinceUpdateDto, Principal principal) {
    Province province = provinceRepository.findById(provinceUpdateDto.getId()).orElseThrow(
        EntityNotFoundException::new);
    boolean checkUpdate = provinceConverter
        .updateDtoToEntity(province, provinceUpdateDto, principal);
    if (checkUpdate) {
      Province updated = provinceRepository.save(province);
      return provinceConverter.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

//  @Override
//  public void deleteById(UUID id) {
//    provinceRepository.deleteById(id);
//  }
//
//  @Override
//  @Cacheable(value = "province", key = "#id")
//  public ProvinceResponseDto getDetailsById(UUID id) {
//    Province province = provinceRepository.findById(id).orElseThrow(
//        EntityNotFoundException::new);
//    return provinceConverter.entityToDtoResponse(province);
//  }

  @Override
  @Cacheable(value = "searchProvince", key = "#searchCriteriaDto")
  public SearchDtoResponse<ProvinceResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<ProvinceResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<Province> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<Province> provinces = provinceRepository
        .findAll(provinceSearchPreProcess.getSpecification(),
            provinceSearchPreProcess.getPageable());
    List<ProvinceResponseDto> provinceResponseDtoList = provinces.getContent().stream().parallel()
        .map(provinceConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(provinces.getTotalPages());
    searchDtoResponse.setTotalElement(provinces.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }
}
