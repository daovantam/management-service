package com.mor.managementservice.service.impl;

import com.mor.managementservice.constant.ResponseStatusCodeEnum;
import com.mor.managementservice.dto.create.CustomerCreateDto;
import com.mor.managementservice.dto.response.CustomerResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteria;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.CustomerUpdateDto;
import com.mor.managementservice.entity.Customer;
import com.mor.managementservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.managementservice.internal.AuthServiceClient;
import com.mor.managementservice.repository.CustomerRepository;
import com.mor.managementservice.search.SpecificationBuilder;
import com.mor.managementservice.service.CustomerService;
import com.mor.managementservice.service.SearchService;
import com.mor.managementservice.utils.converter.CustomerConverter;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

  private final CustomerConverter customerConverter;

  private final CustomerRepository customerRepository;

  private final AuthServiceClient authServiceClient;


  private final SearchService<Customer> searchService;
  private final SpecificationBuilder builder = new SpecificationBuilder(Customer.class);

  @Autowired
  public CustomerServiceImpl(
      CustomerConverter customerConverter,
      CustomerRepository customerRepository,
      AuthServiceClient authServiceClient,
      SearchService<Customer> searchService) {
    this.customerConverter = customerConverter;
    this.customerRepository = customerRepository;
    this.authServiceClient = authServiceClient;
    this.searchService = searchService;
  }

  @Override
  public CustomerResponseDto create(CustomerCreateDto customerCreateDto, Principal principal) {
    Customer customer = customerConverter.createDtoToEntity(customerCreateDto, principal);
    Customer created = customerRepository.save(customer);
    return customerConverter.entityToDtoResponse(created);
  }

  @Override
  public CustomerResponseDto update(CustomerUpdateDto customerUpdateDto, Principal principal) {
    Customer customerExisted = customerRepository.findById(customerUpdateDto.getId())
        .orElseThrow(() -> new EntityNotFoundException("Customer not found"));
    boolean isUpdate = customerConverter
        .updateDtoToEntity(customerExisted, customerUpdateDto, principal);
    if (isUpdate) {
      Customer updated = customerRepository.save(customerExisted);
      return customerConverter.entityToDtoResponse(updated);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

  @Override
  public SearchDtoResponse<CustomerResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    return null;
  }

  @Override
  public void deleteById(UUID id) {
    customerRepository.deleteById(id);
  }

  @Override
  public CustomerResponseDto getDetailsById(UUID id) {
    Customer customer = customerRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Customer not found"));
    return customerConverter.entityToDtoResponse(customer);
  }

  @Override
  public SearchDtoResponse<CustomerResponseDto> searchCustomer(SearchCriteriaDto searchCriteriaDto,
      Principal principal) {
    List<String> hostList = Objects
        .requireNonNull(authServiceClient.getAllHostForSale(principal.getName()).getBody())
        .getData();
    SearchCriteria searchCriteria = new SearchCriteria();
    searchCriteria.setKey("hostName");
    searchCriteria.setOperation("in");
    searchCriteria.setValue(hostList);
    searchCriteria.setOrPredicate(false);
    searchCriteriaDto.getMatching().add(searchCriteria);
    SearchDtoResponse<CustomerResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<Customer> customerSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<Customer> customers = customerRepository
        .findAll(customerSearchPreProcess.getSpecification(),
            customerSearchPreProcess.getPageable());
    List<CustomerResponseDto> customerResponseDtoList = customers.getContent().stream().parallel()
        .map(customerConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(customers.getTotalPages());
    searchDtoResponse.setTotalElement(customers.getTotalElements());
    searchDtoResponse.setListObject(customerResponseDtoList);
    return searchDtoResponse;
  }
}
