package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.ProvinceCreateDto;
import com.mor.managementservice.dto.response.ProvinceResponseDto;
import com.mor.managementservice.dto.update.ProvinceUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface ProvinceService extends
    BaseService<ProvinceCreateDto, ProvinceUpdateDto, ProvinceResponseDto> {

}
