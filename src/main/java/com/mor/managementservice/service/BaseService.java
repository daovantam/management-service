package com.mor.managementservice.service;

import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import java.io.IOException;
import java.security.Principal;

public interface BaseService<C, U, R> {

  R create(C c, Principal principal) throws IOException;

  R update(U u, Principal principal);

  SearchDtoResponse<R> search(SearchCriteriaDto searchCriteriaDto);
}
