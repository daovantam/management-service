package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.HouseCreateDto;
import com.mor.managementservice.dto.response.HouseResponseDto;
import com.mor.managementservice.dto.update.HouseUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface HouseService extends
    BaseService<HouseCreateDto, HouseUpdateDto, HouseResponseDto>, UUIDService<HouseResponseDto> {

}
