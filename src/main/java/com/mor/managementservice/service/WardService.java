package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.WardCreateDto;
import com.mor.managementservice.dto.response.WardResponseDto;
import com.mor.managementservice.dto.update.WardUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface WardService extends BaseService<WardCreateDto, WardUpdateDto, WardResponseDto> {

}
