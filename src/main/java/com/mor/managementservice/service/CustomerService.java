package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.CustomerCreateDto;
import com.mor.managementservice.dto.response.CustomerResponseDto;
import com.mor.managementservice.dto.response.SearchDtoResponse;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.dto.update.CustomerUpdateDto;
import java.security.Principal;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService extends
    BaseService<CustomerCreateDto, CustomerUpdateDto, CustomerResponseDto>,
    UUIDService<CustomerResponseDto> {

  SearchDtoResponse<CustomerResponseDto> searchCustomer(SearchCriteriaDto searchCriteriaDto,
      Principal principal);
}
