package com.mor.managementservice.service;

import com.mor.managementservice.dto.create.DistrictCreateDto;
import com.mor.managementservice.dto.response.DistrictResponseDto;
import com.mor.managementservice.dto.update.DistrictUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface DistrictService extends
    BaseService<DistrictCreateDto, DistrictUpdateDto, DistrictResponseDto> {

}
