package com.mor.managementservice.service;

import com.mor.managementservice.dto.response.SearchPreProcess;
import com.mor.managementservice.dto.search.SearchCriteriaDto;
import com.mor.managementservice.search.SpecificationBuilder;

public interface SearchService<T> {

  SearchPreProcess<T> search(SearchCriteriaDto searchCriteriaDto, SpecificationBuilder builder);
}
