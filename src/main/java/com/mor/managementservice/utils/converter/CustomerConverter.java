package com.mor.managementservice.utils.converter;

import com.mor.managementservice.dto.create.CustomerCreateDto;
import com.mor.managementservice.dto.response.CustomerResponseDto;
import com.mor.managementservice.dto.update.CustomerUpdateDto;
import com.mor.managementservice.entity.Customer;
import com.mor.managementservice.entity.HouseForRent;
import com.mor.managementservice.repository.HouseRepository;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerConverter implements
    BaseConverter<CustomerCreateDto, CustomerUpdateDto, CustomerResponseDto, Customer> {

  private final HouseRepository houseRepository;

  @Autowired
  public CustomerConverter(HouseRepository houseRepository) {
    this.houseRepository = houseRepository;
  }

  @Override
  public Customer createDtoToEntity(CustomerCreateDto customerCreateDto, Principal principal) {
    HouseForRent houseForRent = houseRepository.findById(customerCreateDto.getHouseId())
        .orElseThrow(() -> new EntityNotFoundException("House not found"));
    Customer customer = new Customer();
    customer.setHouseForRent(houseForRent);
    customer.setName(customerCreateDto.getName());
    customer.setPhone(customerCreateDto.getPhone());
    customer.setHostName(houseForRent.getCreatedBy());
    customer.setCreatedBy(principal.getName());
    customer.setUpdatedBy(principal.getName());
    return customer;
  }

  @Override
  public boolean updateDtoToEntity(Customer customer, CustomerUpdateDto customerUpdateDto,
      Principal principal) {
    if (customer.getStatus() != customerUpdateDto.getStatus()) {
      customer.setStatus(customerUpdateDto.getStatus());
      customer.setUpdatedBy(principal.getName());
      return true;
    }
    return false;
  }

  @Override
  public CustomerResponseDto entityToDtoResponse(Customer customer) {
    CustomerResponseDto customerResponseDto = new CustomerResponseDto();
    customerResponseDto.setName(customer.getName());
    customerResponseDto.setPhone(customer.getPhone());
    customerResponseDto.setTitleHouse(customer.getHouseForRent().getTitle());
    customerResponseDto.setHostName(customer.getHostName());
    return customerResponseDto;
  }
}
