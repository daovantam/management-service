package com.mor.managementservice.utils.converter;

import com.mor.managementservice.dto.create.CategoryCreateDto;
import com.mor.managementservice.dto.response.CategoryResponseDto;
import com.mor.managementservice.dto.update.CategoryUpdateDto;
import com.mor.managementservice.entity.Category;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import org.springframework.stereotype.Component;

@Component
public class ConverterCategory implements
    BaseConverter<CategoryCreateDto, CategoryUpdateDto, CategoryResponseDto, Category> {

  @Override
  public Category createDtoToEntity(CategoryCreateDto categoryCreateDto, Principal principal) {
    Category category = new Category();
    category.setDescription(categoryCreateDto.getDescription());
    category.setName(categoryCreateDto.getName());
    category.setCreatedBy(principal.getName());
    category.setUpdatedBy(principal.getName());
    return category;
  }

  @Override
  public boolean updateDtoToEntity(Category category, CategoryUpdateDto categoryUpdateDto,
      Principal principal) {
    boolean checkUpdate = false;
    if (!categoryUpdateDto.getName().equals(category.getName())) {
      checkUpdate = true;
      category.setName(categoryUpdateDto.getName());
    }
    if (!categoryUpdateDto.getDescription().equals(category.getDescription())) {
      checkUpdate = true;
      category.setDescription(categoryUpdateDto.getDescription());
    }
    if (checkUpdate) {
      category.setUpdatedBy(principal.getName());
    }

    return checkUpdate;
  }

  @Override
  public CategoryResponseDto entityToDtoResponse(Category category) {

    CategoryResponseDto responseDto = new CategoryResponseDto();
    responseDto.setId(category.getId());
    responseDto.setName(category.getName());
    responseDto.setDescription(category.getDescription());
    responseDto.setUpdatedTimestamp(category.getUpdatedTimestamp());
    responseDto.setUpdateBy(category.getUpdatedBy());
    responseDto.setImagePath(category.getImage());
    return responseDto;
  }
}
