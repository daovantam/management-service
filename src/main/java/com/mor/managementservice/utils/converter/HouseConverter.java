package com.mor.managementservice.utils.converter;

import com.mor.managementservice.constant.TimelineEnum;
import com.mor.managementservice.dto.create.HouseCreateDto;
import com.mor.managementservice.dto.response.HouseResponseDto;
import com.mor.managementservice.dto.update.HouseUpdateDto;
import com.mor.managementservice.entity.Category;
import com.mor.managementservice.entity.HouseForRent;
import com.mor.managementservice.entity.Ward;
import com.mor.managementservice.repository.CategoryRepository;
import com.mor.managementservice.repository.WardRepository;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HouseConverter implements
    BaseConverter<HouseCreateDto, HouseUpdateDto, HouseResponseDto, HouseForRent> {

  private final WardRepository wardRepository;
  private final CategoryRepository categoryRepository;

  @Autowired
  public HouseConverter(WardRepository wardRepository,
      CategoryRepository categoryRepository) {
    this.wardRepository = wardRepository;
    this.categoryRepository = categoryRepository;
  }

  @Override
  public HouseForRent createDtoToEntity(HouseCreateDto houseCreateDto, Principal principal) {
    Ward ward = checkExistAndGetWard(houseCreateDto.getWardId());
    Category category = checkExistAndGetCategory(houseCreateDto.getCategoryId());
    HouseForRent houseForRent = new HouseForRent();
    houseForRent.setTitle(houseCreateDto.getTitle());
    houseForRent.setWard(ward);
    houseForRent.setPrice(houseCreateDto.getPrice());
    houseForRent.setNumberOfBedroom(houseCreateDto.getNumberOfBedroom());
    houseForRent.setCategory(category);
    houseForRent.setTimeline(TimelineEnum.getValueByTimeline(houseCreateDto.getTimeline()));
    houseForRent.setAcreage(houseCreateDto.getAcreage());
    houseForRent.setDistrictId(ward.getDistrictId());
    houseForRent.setProvinceId(ward.getDistrict().getProvinceId());
    houseForRent.setCreatedBy(principal.getName());
    houseForRent.setUpdatedBy(principal.getName());
    houseForRent.setStatus(true);
    houseForRent.setImagesPath(houseCreateDto.getFilesPath());
    houseForRent.setDescription(houseCreateDto.getDescription());
    return houseForRent;
  }

  @Override
  public boolean updateDtoToEntity(HouseForRent houseForRent, HouseUpdateDto houseUpdateDto,
      Principal principal) {
    boolean checkUpdate = false;
    if (!houseForRent.getCategoryId().toString()
        .equals(houseUpdateDto.getCategoryId().toString())) {
      houseForRent.setCategory(checkExistAndGetCategory(houseUpdateDto.getCategoryId()));
      checkUpdate = true;
    }
    if (!houseForRent.getWardId().equals(houseUpdateDto.getWardId())) {
      Ward ward = checkExistAndGetWard(houseUpdateDto.getWardId());
      houseForRent.setWard(ward);
      houseForRent.setDistrictId(ward.getDistrictId());
      houseForRent.setProvinceId(ward.getDistrict().getProvinceId());
      checkUpdate = true;
    }
    if (houseForRent.getAcreage() != houseUpdateDto.getAcreage()) {
      houseForRent.setAcreage(houseUpdateDto.getAcreage());
      checkUpdate = true;
    }
    if (houseForRent.getNumberOfBedroom() != houseUpdateDto.getNumberOfBedroom()) {
      houseForRent.setNumberOfBedroom(houseUpdateDto.getNumberOfBedroom());
      checkUpdate = true;
    }
    if (houseForRent.getPrice().doubleValue() != houseUpdateDto.getPrice().doubleValue()) {
      houseForRent.setPrice(houseUpdateDto.getPrice());
      checkUpdate = true;
    }
    if (!houseForRent.getTitle().equals(houseUpdateDto.getTitle())) {
      houseForRent.setTitle(houseUpdateDto.getTitle());
      checkUpdate = true;
    }
    if (!houseForRent.getDescription().equals(houseUpdateDto.getDescription())) {
      houseForRent.setDescription(houseUpdateDto.getDescription());
      checkUpdate = true;
    }
    if (!TimelineEnum.getTimelineByValue(houseForRent.getTimeline())
        .equals(houseUpdateDto.getTimeline())) {
      houseForRent.setTimeline(TimelineEnum.getValueByTimeline(houseUpdateDto.getTimeline()));
      checkUpdate = true;
    }
    if (checkUpdate) {
      houseForRent.setUpdatedBy(principal.getName());
    }
    return checkUpdate;
  }

  @Override
  public HouseResponseDto entityToDtoResponse(HouseForRent houseForRent) {
    List<String> imagesList = new ArrayList<>();
    if (houseForRent.getImagesPath() != null) {
      String[] imagesPath = houseForRent.getImagesPath().split(";");
      imagesList = Arrays.asList(imagesPath);
    }
    return HouseResponseDto.builder().id(houseForRent.getId())
        .wardName(houseForRent.getWard().getName()).title(houseForRent.getTitle())
        .wardId(houseForRent.getWard().getId())
        .provinceName(houseForRent.getWard().getDistrict().getProvince().getName())
        .provinceId(houseForRent.getProvinceId())
        .districtName(houseForRent.getWard().getDistrict().getName())
        .districtId(houseForRent.getDistrictId()).price(houseForRent.getPrice())
        .acreage(houseForRent.getAcreage()).numberOfBedroom(houseForRent.getNumberOfBedroom())
        .timeline(TimelineEnum.getTimelineByValue(houseForRent.getTimeline()))
        .categoryName(houseForRent.getCategory().getName()).description(
            houseForRent.getDescription()).imagesPath(imagesList)
        .categoryId(houseForRent.getCategory().getId()).status(houseForRent.isStatus())
        .build();
  }

  private Ward checkExistAndGetWard(String wardId) {
    return wardRepository.findById(wardId)
        .orElseThrow(() -> new EntityNotFoundException("ward not found"));
  }

  private Category checkExistAndGetCategory(UUID categoryId) {
    return categoryRepository.findById(categoryId)
        .orElseThrow(() -> new EntityNotFoundException("category not found"));
  }
}
