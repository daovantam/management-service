package com.mor.managementservice.utils.converter;

import com.mor.managementservice.dto.create.DistrictCreateDto;
import com.mor.managementservice.dto.response.DistrictResponseDto;
import com.mor.managementservice.dto.update.DistrictUpdateDto;
import com.mor.managementservice.entity.District;
import com.mor.managementservice.entity.Province;
import com.mor.managementservice.repository.ProvinceRepository;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DistrictConverter implements
    BaseConverter<DistrictCreateDto, DistrictUpdateDto, DistrictResponseDto, District> {

  public final ProvinceRepository provinceRepository;

  @Autowired
  public DistrictConverter(
      ProvinceRepository provinceRepository) {
    this.provinceRepository = provinceRepository;
  }

  @Override
  public District createDtoToEntity(DistrictCreateDto districtCreateDto, Principal principal) {
    District district = new District();
    district.setProvince(checkExistAndGetProvince(districtCreateDto.getProvinceId()));
    district.setName(districtCreateDto.getName());
    return district;
  }

  @Override
  public boolean updateDtoToEntity(District district, DistrictUpdateDto districtUpdateDto,
      Principal principal) {
    boolean checkUpdate = false;
    if (!districtUpdateDto.getName().isEmpty() && !district.getName()
        .equals(districtUpdateDto.getName())) {
      district.setName(districtUpdateDto.getName());
      checkUpdate = true;
    }
    if (districtUpdateDto.getProvinceId() != null && !district.getProvince().getId()
        .equals(districtUpdateDto.getProvinceId())) {
      Province province = checkExistAndGetProvince(districtUpdateDto.getProvinceId());
      district.setProvince(province);
      checkUpdate = true;
    }
    return checkUpdate;
  }

  @Override
  public DistrictResponseDto entityToDtoResponse(District district) {
    return DistrictResponseDto.builder().id(district.getId()).name(district.getName()).provinceName(
        district.getProvince().getName()).build();
  }

  private Province checkExistAndGetProvince(String uuid) {
    return provinceRepository.findById(uuid).orElseThrow(EntityNotFoundException::new);
  }
}
