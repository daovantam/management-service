package com.mor.managementservice.utils.converter;

import com.mor.managementservice.dto.create.ProvinceCreateDto;
import com.mor.managementservice.dto.response.ProvinceResponseDto;
import com.mor.managementservice.dto.update.ProvinceUpdateDto;
import com.mor.managementservice.entity.Province;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import org.springframework.stereotype.Component;

@Component
public class ProvinceConverter implements
    BaseConverter<ProvinceCreateDto, ProvinceUpdateDto, ProvinceResponseDto, Province> {

  @Override
  public Province createDtoToEntity(ProvinceCreateDto provinceCreateDto, Principal principal) {
    Province province = new Province();
    province.setName(provinceCreateDto.getName());
    return province;
  }

  @Override
  public boolean updateDtoToEntity(Province province, ProvinceUpdateDto provinceUpdateDto,
      Principal principal) {
    boolean checkUpdate = false;
    if (!provinceUpdateDto.getName().equals(province.getName())) {
      checkUpdate = true;
      provinceUpdateDto.setName(provinceUpdateDto.getName());
    }

    return checkUpdate;
  }

  @Override
  public ProvinceResponseDto entityToDtoResponse(Province province) {
    ProvinceResponseDto responseDto = new ProvinceResponseDto();
    responseDto.setId(province.getId());
    responseDto.setName(province.getName());
    responseDto.setType(province.getType());
    return responseDto;
  }
}
