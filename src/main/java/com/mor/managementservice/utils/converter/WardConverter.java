package com.mor.managementservice.utils.converter;

import com.mor.managementservice.dto.create.WardCreateDto;
import com.mor.managementservice.dto.response.WardResponseDto;
import com.mor.managementservice.dto.update.WardUpdateDto;
import com.mor.managementservice.entity.District;
import com.mor.managementservice.entity.Ward;
import com.mor.managementservice.repository.DistrictRepository;
import com.mor.managementservice.utils.BaseConverter;
import java.security.Principal;
import javax.persistence.EntityNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class WardConverter implements
    BaseConverter<WardCreateDto, WardUpdateDto, WardResponseDto, Ward> {

  private final DistrictRepository districtRepository;

  public WardConverter(DistrictRepository districtRepository) {
    this.districtRepository = districtRepository;
  }

  @Override
  public Ward createDtoToEntity(WardCreateDto wardCreateDto, Principal principal) {
    District district = checkExistAndGetDistrict(wardCreateDto.getDistrictId());
    Ward ward = new Ward();
    ward.setName(wardCreateDto.getName());
    ward.setDistrict(district);
    return ward;
  }

  @Override
  public boolean updateDtoToEntity(Ward ward, WardUpdateDto wardUpdateDto, Principal principal) {
    boolean checkUpdate = false;
    District district = checkExistAndGetDistrict(wardUpdateDto.getDistrictId());
    if (!wardUpdateDto.getName().equals(ward.getName())) {
      checkUpdate = true;
      ward.setName(wardUpdateDto.getName());
    }
    if (!ward.getDistrict().getId().equals(wardUpdateDto.getDistrictId())) {
      checkUpdate = true;
      ward.setDistrict(district);
    }

    return checkUpdate;
  }

  @Override
  public WardResponseDto entityToDtoResponse(Ward ward) {
    return WardResponseDto.builder().id(ward.getId()).name(ward.getName())
        .districtName(ward.getDistrict().getName())
        .provinceName(ward.getDistrict().getProvince().getName()).build();
  }

  private District checkExistAndGetDistrict(String uuid) {
    return districtRepository.findById(uuid).orElseThrow(EntityNotFoundException::new);
  }
}
