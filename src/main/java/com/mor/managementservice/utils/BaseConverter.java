package com.mor.managementservice.utils;

import java.security.Principal;

public interface BaseConverter<C, U, R, E> {

  E createDtoToEntity(C c, Principal principal);

  boolean updateDtoToEntity(E e, U u, Principal principal);

  R entityToDtoResponse(E e);
}
