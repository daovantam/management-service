package com.mor.managementservice.repository;

import com.mor.managementservice.entity.Ward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface WardRepository extends JpaRepository<Ward, String>,
    JpaSpecificationExecutor<Ward> {

}
