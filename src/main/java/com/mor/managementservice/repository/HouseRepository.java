package com.mor.managementservice.repository;

import com.mor.managementservice.entity.HouseForRent;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface HouseRepository extends JpaRepository<HouseForRent, UUID>,
    JpaSpecificationExecutor<HouseForRent> {

}
