package com.mor.managementservice.repository;

import com.mor.managementservice.entity.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, String>,
    JpaSpecificationExecutor<Province> {

}
